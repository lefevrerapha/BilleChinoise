﻿using BilleChinoiseGame.Structure_description.Player;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BilleChinoiseGame
{
    public class GraphicalDisplay
    {
        #region propertie
        public Game game { get; }

        Panel panel { get; }
        Label label { get; }
        Button AiActionButton { get; }
        public Graphics graphic { get; set; }
        public int tableHeight { get { return panel.Height; } }
        public int tableWidth { get { return panel.Width; } }
        static public Brush coul_case_ext = Brushes.White;
        static public Brush coul_case_ext_selected = Brushes.Red;
        static public Brush coul_case_ext_autorised = Brushes.Chocolate;
        static public Brush coul_case_int = Brushes.Gray;
        static public Brush textBrush = Brushes.Black;
        public int z = 2;
        public int u = 8;
        public int externalRayon = 10;
        public int internalRayon = 8;
        int offset = 3;//in %
        private object lockDraw = new object();
        #endregion

        #region constructor
        public GraphicalDisplay(Game game, Panel panel, Label label, Button AiActionButton)
        {
            this.game = game;
            this.panel = panel;
            this.label = label;
            this.AiActionButton = AiActionButton;
        }
        #endregion

        #region method
        public void InitDrawing()
        {
            graphic = panel.CreateGraphics();
            graphic.SmoothingMode = SmoothingMode.AntiAlias;
            DisplayPlayer(game.currentPlayer);
        }
        public void DisplayPlayer(IPlayer currentPlayer)
        {
            label.Text = currentPlayer.name + " score: " + currentPlayer.GetScore().ToString();
            label.BackColor = ((SolidBrush)currentPlayer.color).Color;
            if (currentPlayer is AiPlayer)
                AiActionButton.Visible = true;
            else
                AiActionButton.Visible = false;
        }
        public void Afficher()
        {
            if (!(game.Plateau is null))
                DiplayPlateau();
        }
        public void DiplayPlateau()
        {
            lock (lockDraw)
                graphic.Clear(panel.BackColor);

            for (int row = 0; row < game.Plateau.Rows; row++)
            {
                for (int column = 0; column < game.Plateau.Columns; column++)
                {
                    Case myCase = game.Plateau.currentMatrix[row, column];
                    if (!(myCase is null))
                        Draw_Case(myCase);
                }
            }
        }
        public void Draw_Case(Case myCase)
        {
            GetCaseCoordinate(myCase, out int centerX, out int centerY);

            Brush brush = myCase.Bille is null ? coul_case_int : myCase.Bille.Player.color;

            //display external countour of the ball
            lock (lockDraw)
            {
                {
                    if (myCase.selected)
                        graphic.FillEllipse(coul_case_ext_selected, centerX - externalRayon, centerY - externalRayon, 2 * externalRayon, 2 * externalRayon);
                    else
                        graphic.FillEllipse(coul_case_ext, centerX - externalRayon, centerY - externalRayon, 2 * externalRayon, 2 * externalRayon);
                    if (myCase.autorised)
                        graphic.FillEllipse(coul_case_ext_autorised, centerX - externalRayon, centerY - externalRayon, 2 * externalRayon, 2 * externalRayon);
                }

                //draw midlle of the case
                graphic.FillEllipse(brush, centerX - internalRayon, centerY - internalRayon, 2 * internalRayon, 2 * internalRayon);

                //display bille number
                if (!(myCase.Bille is null))
                    graphic.DrawString(myCase.Bille.numberBille.ToString(), SystemFonts.CaptionFont, textBrush, centerX - 2 * internalRayon / 3, centerY - internalRayon);
            }
        }

        public void FocusCase(int x, int y)
        {
            GetCaseFromCoordinate(x, y, out Case focusCase);
            game.Plateau.selectedCaseSucced = (!(focusCase is null), focusCase);
            return;
        }

        public void celebrate(List<HumanPlayer> players)
        {
            HumanPlayer winner = players.Where(x => x.finish).FirstOrDefault();
            label.Text = "Le joueur " + winner.name + " a gagné!!";
            label.BackColor = ((SolidBrush)winner.color).Color;
        }

        public void GetCaseCoordinate(Case myCase, out int centerX, out int centerY)
        {
            centerX = (int)(myCase.column * tableWidth * (1 - 2 * (double)offset / 100) / game.Plateau.Columns + tableWidth * (double)offset / 100);
            centerY = (int)(myCase.row * tableHeight * (1 - 2 * (double)offset / 100) / game.Plateau.Rows + tableHeight * (double)offset / 100);
        }
        private void GetCaseFromCoordinate(int x, int y, out Case selectedCase)
        {
            selectedCase = null;
            int Row = (int)Math.Round((y - tableHeight * (double)offset / 100) * game.Plateau.Rows / (tableHeight * (1 - 2 * (double)offset / 100)));
            int Column = (int)Math.Round((x - tableWidth * (double)offset / 100) * game.Plateau.Columns / (tableWidth * (1 - 2 * (double)offset / 100)));

            if (Column >= game.Plateau.Columns || Column < 0)
                return;
            if (Row >= game.Plateau.Rows || Row < 0)
                return;

            selectedCase = game.Plateau.currentMatrix[Row, Column];
        }

        #endregion
    }
}
