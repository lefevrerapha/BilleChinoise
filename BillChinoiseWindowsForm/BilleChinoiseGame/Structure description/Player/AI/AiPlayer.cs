﻿using BilleChinoiseGame.Structure_description.Player;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BilleChinoiseGame
{
    public class AiPlayer : HumanPlayer
    {
        #region properties
        public enum AiType
        {
            Random = 0,
            Raph

        }
        static private Random rnd = new Random();
        public AiType aiType { get; } = AiType.Random;
        public int Attempt { get; set; } = 0;
        #endregion

        #region constructor
        public AiPlayer(int numberPlayer, bool active, HumanPlayer oppositePLayer, string name = "No name", AiType aiType = AiType.Random) : base(numberPlayer, active, oppositePLayer, name)
        {
            this.aiType = aiType;
        }
        #endregion

        #region Method

        public override bool Move(GraphicalDisplay graphicalDisplay, bool canFail)
        {
            List<Case> caseToRedrawList = new List<Case>();

            Bille selectedFirstBille = null;
            Case selectedDestinationCase = null;
            while (selectedFirstBille is null || selectedDestinationCase is null || graphicalDisplay.game.Plateau.selectedCaseSucced.succed != null)
            {
                AiAction(graphicalDisplay, out int x, out int y);
                graphicalDisplay.FocusCase(x, y);

                Case selectedCase = graphicalDisplay.game.Plateau.selectedCaseSucced.selectedCase;
                if (!canFail)
                {
                    if (!graphicalDisplay.game.Plateau.selectedCaseSucced.succed.Value)
                        return false;
                    if (selectedFirstBille is null && (selectedCase is null || selectedCase.Bille is null || !selectedCase.Bille.Player.Equals(this)))
                        return false;

                    if (selectedDestinationCase is null && (selectedCase is null || !selectedCase.autorised))
                        return false;
                }

                if (!(selectedCase is null) && !(selectedCase.Bille is null) && selectedCase.Bille.Player.Equals(this))
                {

                    if (!(selectedFirstBille is null))
                    {
                        selectedFirstBille.Case.selected = false;
                        caseToRedrawList.Add(selectedFirstBille.Case);
                    }
                    selectedFirstBille = selectedCase.Bille;
                    graphicalDisplay.game.selectNewBille(selectedCase, ref caseToRedrawList);
                    //caseToRedrawList.ForEach(m => graphicalDisplay.Draw_Case(m));
                }

                if (!(selectedCase is null) && !(selectedFirstBille is null) && selectedCase.autorised)
                    selectedDestinationCase = selectedCase;

                graphicalDisplay.game.Plateau.selectedCaseSucced = (null, null);

            }
            //do the move
            graphicalDisplay.game.moveBille(selectedFirstBille, selectedDestinationCase, out caseToRedrawList);
            caseToRedrawList.ForEach(m => graphicalDisplay.Draw_Case(m));

            Attempt = 0;
            return true;
        }
        public void AiAction(GraphicalDisplay graphicalDisplay, out int x, out int y)
        {
            x = 0; y = 0;
            if (aiType == AiType.Random)
            {
                int maxHeight = graphicalDisplay.tableHeight;
                int maxWidth = graphicalDisplay.tableWidth;

                x = rnd.Next(0, maxWidth);
                y = rnd.Next(0, maxHeight);
            }
            else if (aiType == AiType.Raph)
            {
                Point finalGravityCenter = new Point(finaleCaseList.Sum(a => a.column) / finaleCaseList.Count, finaleCaseList.Sum(a => a.row) / finaleCaseList.Count);
                Bille bestBille = null;
                Case newCase = null;
                double score = this.GetScore();
                foreach (Bille bille in billes)
                {
                    bestBille = bille;
                    List<Case> cases = graphicalDisplay.game.Plateau.AutorisedCaseList(bille.Case);
                    foreach (Case potentailCase in cases)
                    {
                        
                        List<Case> casesConfig = new List<Case>();
                        billes.ForEach((item) =>
                        {
                            if (bille == item)
                                casesConfig.Add(potentailCase);
                            else
                                casesConfig.Add(item.Case);
                        });
                        Point potentialFuturStat = new Point(casesConfig.Sum(c => c.column) / finaleCaseList.Count, casesConfig.Sum(c => c.row) / finaleCaseList.Count);

                        double distance = Math.Sqrt(Math.Pow(finalGravityCenter.X - potentialFuturStat.X, 2) + Math.Pow(finalGravityCenter.Y - potentialFuturStat.Y, 2));
                        if (distance < score)
                        {
                            newCase = potentailCase;
                            break;
                        }

                    }
                    if (newCase != null)
                        break;

                }
                if (!billes.Any(b => b.Case.selected))
                {
                    graphicalDisplay.GetCaseCoordinate(bestBille.Case, out x, out y);
                }
                else
                {
                    graphicalDisplay.GetCaseCoordinate(newCase, out x, out y);
                }
            }
            Attempt++;
        }
        #endregion
    }
}
