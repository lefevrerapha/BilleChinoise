﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilleChinoiseGame.Structure_description.Player
{
    public interface IPlayer
    {
        #region properties
        int numberPlayer { get; }
        string name { get; }
        bool active { get; }
        bool finish { get; set; }
        List<Bille> billes { get; set; }
        Brush color { get; }
        HumanPlayer oppositePlayer { get; set; }
        List<Case> finaleCaseList { get; set; }
        #endregion

        #region method
        double GetScore();
        bool Move(GraphicalDisplay graphicalDisplay, bool canFail);
        #endregion
    }
}
