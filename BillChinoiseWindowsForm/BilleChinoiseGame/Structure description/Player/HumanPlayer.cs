﻿using BilleChinoiseGame.Structure_description.Player;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BilleChinoiseGame
{
    public class HumanPlayer : IPlayer
    {
        #region properties
        public int numberPlayer { get; }// de 1 à 6
        public string name { get; } = "No human name";
        public bool active { get; } = false;
        public bool finish { get; set; } = false;
        public List<Bille> billes { get; set; } = new List<Bille>();
        public Brush color { get; } = Brushes.Black;
        public HumanPlayer oppositePlayer { get; set; } = null;
        public List<Case> finaleCaseList { get; set; } = new List<Case>();
        #endregion

        #region constructore
        public HumanPlayer(int numberPlayer, bool active, HumanPlayer oppositePLayer, string name = "No name")
        {
            this.numberPlayer = numberPlayer;
            this.name = name;

            this.active = active;
            finish = false;
            for (int i = 0; i < Game.maxBilleNumberPerPlayer; i++)
                billes.Add(new Bille(this, null, i));

            if (numberPlayer == 1)
                color = Brushes.Yellow;
            else if (numberPlayer == 2)
                color = Brushes.LightGreen;
            else if (numberPlayer == 3)
                color = Brushes.Blue;
            else if (numberPlayer == 4)
                color = Brushes.Orange;
            else if (numberPlayer == 5)
                color = Brushes.Red;
            else if (numberPlayer == 6)
                color = Brushes.Violet;
            this.oppositePlayer = oppositePLayer;
            if (!(oppositePLayer is null))
                oppositePLayer.oppositePlayer = this;
        }
        #endregion

        #region method 
        public virtual bool Move(GraphicalDisplay graphicalDisplay, bool canFail = true)
        {
            List<Case> caseToRedrawList = new List<Case>();

            Bille selectedFirstBille = null;
            Case selectedDestinationCase = null;
            while (selectedFirstBille is null || selectedDestinationCase is null)
            {
                while (graphicalDisplay.game.Plateau.selectedCaseSucced.succed == null)//wait while no selection have been done
                {
                    Thread.Sleep(10);
                }
                Case selectedCase = graphicalDisplay.game.Plateau.selectedCaseSucced.selectedCase;
                if (!canFail)
                {
                    if (!graphicalDisplay.game.Plateau.selectedCaseSucced.succed.Value)
                        return false;
                    if (selectedFirstBille is null && (selectedCase is null || selectedCase.Bille is null || !selectedCase.Bille.Player.Equals(this)))
                        return false;

                    if (selectedDestinationCase is null && (selectedCase is null || !selectedCase.autorised))
                        return false;
                }

                if (!(selectedCase is null) && !(selectedCase.Bille is null) && selectedCase.Bille.Player.Equals(this))
                {
              
                    if (!(selectedFirstBille is null))
                    {
                        selectedFirstBille.Case.selected = false;
                        caseToRedrawList.Add(selectedFirstBille.Case);
                    }
                    selectedFirstBille = selectedCase.Bille;
                    graphicalDisplay.game.selectNewBille(selectedCase, ref caseToRedrawList);
                    caseToRedrawList.ForEach(m => graphicalDisplay.Draw_Case(m));

                }

                if (!(selectedCase is null) && !(selectedFirstBille is null) && selectedCase.autorised)
                    selectedDestinationCase = selectedCase;

                graphicalDisplay.game.Plateau.selectedCaseSucced = (null, null);
                Thread.Sleep(10);
            }
            //do the move
            graphicalDisplay.game.moveBille(selectedFirstBille, selectedDestinationCase, out caseToRedrawList);
            caseToRedrawList.ForEach(m => graphicalDisplay.Draw_Case(m));
            return true;
        }

        #region override
        public override bool Equals(object obj)
        {
            return obj is HumanPlayer player &&
                   numberPlayer == player.numberPlayer;
        }
        public double GetScore()
        {
            Point finalGravityCenter = new Point(finaleCaseList.Sum(x => x.column) / finaleCaseList.Count, finaleCaseList.Sum(x => x.row) / finaleCaseList.Count);
            Point currentGravityCenter = new Point(billes.Select(x => x.Case).Sum(x => x.column) / finaleCaseList.Count, billes.Select(x => x.Case).Sum(x => x.row) / finaleCaseList.Count);

            double distance = Math.Sqrt(Math.Pow(finalGravityCenter.X - currentGravityCenter.X, 2) + Math.Pow(finalGravityCenter.Y - currentGravityCenter.Y, 2));
            return distance;
        }

        public override string ToString()
        {
            return name;
        }


        #endregion

        #endregion
    }
}
