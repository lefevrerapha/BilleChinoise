﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BilleChinoiseGame
{
    public class Game
    {
        #region properties
        private static int maxPLayerNumber = 6;
        public static int maxBilleNumberPerPlayer = 10;
        public List<HumanPlayer> players { get; } = new List<HumanPlayer>();
        public HumanPlayer currentPlayer { get; set; }
        public Plateau Plateau { get; }
        public List<GameState> gameStates { get; set; } = new List<GameState>();
        public GraphicalDisplay graphicalDisplay { get; set; }
        #endregion

        #region constructor
        public Game(int playerNumber, int AiPlayerNumber)
        {
            HumanPlayer previousPlayer = null;
            for (int i = 0; i < playerNumber; i++)
            {
                HumanPlayer currentPLayer = new HumanPlayer(i + 1, true, previousPlayer, "player_" + (i + 1));
                players.Add(currentPLayer);
                previousPlayer = currentPLayer;
            }
            for (int i = playerNumber; i < AiPlayerNumber + playerNumber; i++)
            {
                HumanPlayer currentPLayer = new AiPlayer(i + 1, true, previousPlayer, "player_" + (i + 1));
                players.Add(currentPLayer);
                previousPlayer = currentPLayer;
            }
            for (int i = playerNumber + AiPlayerNumber; i < maxPLayerNumber; i++)
            {
                HumanPlayer currentPLayer = new HumanPlayer(i + 1, false, previousPlayer);
                players.Add(currentPLayer);
                previousPlayer = currentPLayer;
            }


            Plateau = new Plateau(players);

            players.ForEach(x => { if (x.active) x.finaleCaseList = x.oppositePlayer.billes.Select(y => y.Case).ToList(); });

            currentPlayer = players.Where(x => x.active).FirstOrDefault();
            gameStates.Add(new GameState(Plateau.currentMatrix, currentPlayer));
        }
        #endregion

        #region method
        public async void Play()
        {
            while (!PlayerWon())
            {
                NextPlayer();
                graphicalDisplay.DisplayPlayer(currentPlayer);
                await Task.Run(() =>
                {
                    currentPlayer.Move(graphicalDisplay, true);
                    currentPlayer.finish = true;
                });
            }
            graphicalDisplay.celebrate(players);

        }
        private void NextPlayer()
        {
            //reset selectedcase
            graphicalDisplay.game.Plateau.selectedCaseSucced = (null, null);
            currentPlayer = players.Where(y => y.numberPlayer == currentPlayer.numberPlayer + 1).FirstOrDefault();
            while (currentPlayer is null || !currentPlayer.active)
            {
                if (currentPlayer is null)
                    currentPlayer = players.FirstOrDefault();
                else
                    currentPlayer = players.Where(y => y.numberPlayer == currentPlayer.numberPlayer + 1).FirstOrDefault();
            }
            gameStates.Add(new GameState(Plateau.currentMatrix, currentPlayer));
        }

        #region action
     
        public void PreviousState()
        {
            GameState previousGameState = gameStates[gameStates.Count - 1];
            gameStates.Remove(previousGameState);

            Plateau.currentMatrix = previousGameState.currentMatrix;
            currentPlayer = previousGameState.currentPlayer;
        }

        private bool PlayerWon()
        {
            return currentPlayer.billes.All(x => currentPlayer.finaleCaseList.Contains(x.Case));
        }

        public void moveBille(Bille bille, Case focusCase, out List<Case> caseToRedrawList)
        {
            caseToRedrawList = new List<Case>();
            bille.Case.selected = false;
            focusCase.selected = false;
            caseToRedrawList.Add(bille.Case);
            bille.moveBille(focusCase);
            caseToRedrawList.Add(focusCase);

            Plateau.autorisedCaseList.ForEach(c => c.autorised = false);
            caseToRedrawList.AddRange(Plateau.autorisedCaseList);
            Plateau.autorisedCaseList = new List<Case>();
            caseToRedrawList = caseToRedrawList.Distinct().ToList();
        }

        public void selectNewBille(Case focusCase, ref List<Case> caseToRedrawList)
        {          
            Plateau.autorisedCaseList.ForEach(c => c.autorised = false);
            caseToRedrawList.AddRange(Plateau.autorisedCaseList);

            //new state
            focusCase.selected = true;
            List<Case> autorisedCaseList = Plateau.AutorisedCaseList(focusCase);

            //draw
            caseToRedrawList.Add(focusCase);
            caseToRedrawList.AddRange(autorisedCaseList);
            caseToRedrawList = caseToRedrawList.Distinct().ToList();
            return;
        }
        #endregion

        #endregion
    }

    public class GameState
    {
        public Case[,] currentMatrix;
        public HumanPlayer currentPlayer;

        public GameState(Case[,] currentMatrix, HumanPlayer currentPlayer)
        {
            this.currentMatrix = currentMatrix;
            this.currentPlayer = currentPlayer;
        }
    }
}
