﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BilleChinoiseGame
{
    public class Plateau
    {
        #region properties
        //Tableau contenant les info des cases du damiers
        // Damier="0" => case vide
        // Damier="1" à "6" => case contenant une bille du joueur correspondant
        // Damier="9" => Contour du damier sans case
        public static int[,] originMatrix { get; } = new int[17, 25]
        {
            {9,9,9,9,9,9,9,9,9,9,9,9,1,9,9,9,9,9,9,9,9,9,9,9,9},
            {9,9,9,9,9,9,9,9,9,9,9,1,9,1,9,9,9,9,9,9,9,9,9,9,9},
            {9,9,9,9,9,9,9,9,9,9,1,9,1,9,1,9,9,9,9,9,9,9,9,9,9},
            {9,9,9,9,9,9,9,9,9,1,9,1,9,1,9,1,9,9,9,9,9,9,9,9,9},
            {3,9,3,9,3,9,3,9,0,9,0,9,0,9,0,9,0,9,6,9,6,9,6,9,6},
            {9,3,9,3,9,3,9,0,9,0,9,0,9,0,9,0,9,0,9,6,9,6,9,6,9},
            {9,9,3,9,3,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,6,9,6,9,9},
            {9,9,9,3,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,6,9,9,9},
            {9,9,9,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,9,9,9},
            {9,9,9,5,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,4,9,9,9},
            {9,9,5,9,5,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,4,9,4,9,9},
            {9,5,9,5,9,5,9,0,9,0,9,0,9,0,9,0,9,0,9,4,9,4,9,4,9},
            {5,9,5,9,5,9,5,9,0,9,0,9,0,9,0,9,0,9,4,9,4,9,4,9,4},
            {9,9,9,9,9,9,9,9,9,2,9,2,9,2,9,2,9,9,9,9,9,9,9,9,9},
            {9,9,9,9,9,9,9,9,9,9,2,9,2,9,2,9,9,9,9,9,9,9,9,9,9},
            {9,9,9,9,9,9,9,9,9,9,9,2,9,2,9,9,9,9,9,9,9,9,9,9,9},
            {9,9,9,9,9,9,9,9,9,9,9,9,2,9,9,9,9,9,9,9,9,9,9,9,9},
        };
        public Case[,] currentMatrix { get; set; } = new Case[17, 25];
        public int Rows { get { return currentMatrix.GetLength(0); } }
        public int Columns { get { return currentMatrix.GetLength(1); } }
        public (bool? succed, Case selectedCase) selectedCaseSucced { get; set; } = (null, null);
      
        public List<Case> autorisedCaseList { get; set; } = new List<Case>();
        #endregion

        #region constructor
        public Plateau(List<HumanPlayer> players)
        {
            int caseCounter = 0;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    int originMatrixValue = originMatrix[i, j];
                    if (originMatrixValue == 9)
                        currentMatrix[i, j] = null;
                    else if (originMatrixValue == 0)
                    {
                        caseCounter++;
                        currentMatrix[i, j] = new Case(caseCounter, null, i, j);
                    }
                    else
                    {
                        HumanPlayer player = players.Where(x => x.numberPlayer == originMatrixValue).FirstOrDefault();
                        Bille bille = player.billes.Where(x => x.Case is null).FirstOrDefault();
                        caseCounter++;
                        currentMatrix[i, j] = new Case(caseCounter, bille, i, j);
                        bille.Case = currentMatrix[i, j];
                    }
                }
            }
        }

        #endregion

        #region method

        #region public
        public List<Case> AutorisedCaseList(Case selectedCase)
        {
            List<Case> AutorisedCaseList = new List<Case>();
            AutorisedCaseList.AddRange(SimplePossibleMove(selectedCase));

            AutorisedCaseList.AddRange(ComplexPossibleMove(selectedCase));
            autorisedCaseList = AutorisedCaseList;
            return AutorisedCaseList;
        }

        private List<Case> ComplexPossibleMove(Case selectedCase, List<Case> initList = null)
        {
            List<Case> ComplexPossibleCaseList;
            if (initList is null)
                ComplexPossibleCaseList = new List<Case>();
            else
                ComplexPossibleCaseList = initList;

            for (int i = -1; i < 2; i++)
            {
                for (int j = -2; j < 3; j++)
                {
                    int simpleI = Math.Min(Rows - 1, Math.Max(0, selectedCase.row + i));
                    int simpleJ = Math.Min(Columns - 1, Math.Max(0, selectedCase.column + j));

                    Case myCase = currentMatrix[simpleI, simpleJ];

                    if (!(myCase is null) && !(myCase.Bille is null))
                    {
                        int complexI = Math.Min(Rows - 1, Math.Max(0, selectedCase.row + (myCase.row - selectedCase.row) * 2));
                        int complexJ = Math.Min(Columns - 1, Math.Max(0, selectedCase.column + (myCase.column - selectedCase.column) * 2));

                        Case ComplexCase = currentMatrix[complexI, complexJ];
                        if (!(ComplexCase is null) && (ComplexCase.Bille is null))
                        {
                            if (!ComplexPossibleCaseList.Contains(ComplexCase))
                            {
                                ComplexCase.autorised = true;
                                ComplexPossibleCaseList.Add(ComplexCase);
                                List<Case> subItems = ComplexPossibleMove(ComplexCase, ComplexPossibleCaseList);
                                ComplexPossibleCaseList.AddRange(subItems);
                                ComplexPossibleCaseList = ComplexPossibleCaseList.Distinct().ToList();
                            }
                        }
                    }
                }
            }
            return ComplexPossibleCaseList;
        }

        private List<Case> SimplePossibleMove(Case selectedCase)
        {
            List<Case> SimplePossibleCaseList = new List<Case>();
            for (int i = -1; i < 2; i++)
            {
                for (int j = -2; j < 3; j++)
                {
                    int matrixI = Math.Min(Rows - 1, Math.Max(0, selectedCase.row + i));
                    int matrixJ = Math.Min(Columns - 1, Math.Max(0, selectedCase.column + j));

                    Case myCase = currentMatrix[matrixI, matrixJ];
                    if (!(myCase is null) && myCase.Bille is null)
                    {
                        myCase.autorised = true;
                        SimplePossibleCaseList.Add(myCase);
                    }
                }
            }
            return SimplePossibleCaseList;
        }
        #endregion

        #endregion
    }
}
