﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using Brush = System.Drawing.Brush;
using Brushes = System.Drawing.Brushes;

namespace BilleChinoiseGame
{
    public class Case
    {
        #region properties
        public Bille Bille { get; set; } = null;
        public int column { get; }
        public int row { get; }
        int numberCase { get; }// de 1 à 121
        public bool selected { get; set; } = false;
        public bool autorised { get; set; } = false;



        #endregion

        #region construcotr
        public Case(int caseNumber, Bille bille, int row, int column)
        {
            Bille = bille;
            numberCase = caseNumber;
            this.column = column;
            this.row = row;
        }
        #endregion

        #region method
       
        #region override
        public override string ToString()
        {
            return column + ";" + row;
        }
        public override bool Equals(object obj)
        {
            return obj is Case @case &&
                   numberCase == @case.numberCase;
        }
        #endregion
        #endregion
    }
}
