﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilleChinoiseGame
{
    public class Bille
    {
        #region properties
        public HumanPlayer Player { get; set; }
        public Case Case { get; set; } = null;
        public int numberBille { get; }// de 0 à 9

        #endregion

        #region constructor
        public Bille(HumanPlayer player, Case mycase, int numberBille)
        {
            Player = player;
            Case = mycase;
            this.numberBille = numberBille;
        }
        #endregion

        #region method
        internal void moveBille(Case focusCase)
        {
            Case.Bille = null;
            Case = focusCase;
            focusCase.Bille = this;
        }

        public override string ToString()
        {
            return Player.ToString() + " bille " + numberBille + ", " + Case.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is Bille bille &&
                  Player == bille.Player &&
                  Case == bille.Case;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #region override

        #endregion
        #endregion
    }
}
