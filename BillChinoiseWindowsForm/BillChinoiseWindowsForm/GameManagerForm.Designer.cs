﻿namespace BillChinoiseWindowsForm
{
    partial class GameManagerForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuNew = new System.Windows.Forms.MenuItem();
            this.menuItem = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.AiActionButton = new System.Windows.Forms.Button();
            this.previousState = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuNew
            // 
            this.menuNew.Index = 0;
            this.menuNew.Text = "Nouvelle Partie";
            this.menuNew.Click += new System.EventHandler(this.Menu_New);
            // 
            // menuItem
            // 
            this.menuItem.DefaultItem = true;
            this.menuItem.Index = 0;
            this.menuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuNew,
            this.menuItem4});
            this.menuItem.Text = "Fichier";
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 1;
            this.menuItem4.Text = "Quitter";
            this.menuItem4.Click += new System.EventHandler(this.Menu_Quitter);
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem});
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(622, 640);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Yellow;
            this.label1.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(472, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 40);
            this.label1.TabIndex = 3;
            this.label1.Text = "Au joueur 1 de jouer";
            // 
            // AiActionButton
            // 
            this.AiActionButton.Location = new System.Drawing.Point(656, 30);
            this.AiActionButton.Name = "AiActionButton";
            this.AiActionButton.Size = new System.Drawing.Size(96, 34);
            this.AiActionButton.TabIndex = 2;
            this.AiActionButton.Text = "AiAction";
            this.AiActionButton.UseVisualStyleBackColor = true;
            this.AiActionButton.Visible = false;
            this.AiActionButton.Click += new System.EventHandler(this.AiActionButton_Click);
            // 
            // previousState
            // 
            this.previousState.Location = new System.Drawing.Point(656, 80);
            this.previousState.Name = "previousState";
            this.previousState.Size = new System.Drawing.Size(96, 34);
            this.previousState.TabIndex = 3;
            this.previousState.Text = "Previous";
            this.previousState.UseVisualStyleBackColor = true;
            this.previousState.Click += new System.EventHandler(this.previousState_Click);
            // 
            // GameManagerForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(772, 677);
            this.Controls.Add(this.previousState);
            this.Controls.Add(this.AiActionButton);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Menu = this.mainMenu;
            this.Name = "GameManagerForm";
            this.Text = "Dames Chinoises";
            this.Load += new System.EventHandler(this.GameManagerForm_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MainMenu mainMenu;
        private System.Windows.Forms.MenuItem menuItem;
        private System.Windows.Forms.MenuItem menuNew;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AiActionButton;
        private System.Windows.Forms.Button previousState;
    }
}

