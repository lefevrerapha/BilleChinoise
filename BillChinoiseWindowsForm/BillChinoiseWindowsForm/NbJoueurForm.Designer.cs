﻿namespace BillChinoiseWindowsForm
{
    partial class NbJoueurForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label = new System.Windows.Forms.Label();
            this.radioButton = new System.Windows.Forms.RadioButton();
            this.button = new System.Windows.Forms.Button();
            this.groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.radioButton6);
            this.groupBox.Controls.Add(this.radioButton5);
            this.groupBox.Controls.Add(this.label6);
            this.groupBox.Controls.Add(this.label5);
            this.groupBox.Controls.Add(this.label3);
            this.groupBox.Controls.Add(this.radioButton3);
            this.groupBox.Controls.Add(this.label4);
            this.groupBox.Controls.Add(this.radioButton4);
            this.groupBox.Controls.Add(this.label2);
            this.groupBox.Controls.Add(this.radioButton2);
            this.groupBox.Controls.Add(this.label);
            this.groupBox.Controls.Add(this.radioButton);
            this.groupBox.Location = new System.Drawing.Point(40, 22);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(128, 164);
            this.groupBox.TabIndex = 0;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "Joueurs";
            // 
            // radioButton6
            // 
            this.radioButton6.Location = new System.Drawing.Point(88, 141);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(16, 15);
            this.radioButton6.TabIndex = 11;
            this.radioButton6.Text = "radioButton";
            // 
            // radioButton5
            // 
            this.radioButton5.Location = new System.Drawing.Point(88, 119);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(16, 15);
            this.radioButton5.TabIndex = 10;
            this.radioButton5.Text = "radioButton";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(16, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 15);
            this.label6.TabIndex = 9;
            this.label6.Text = "6 Joueurs";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "5 Joueurs";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 14);
            this.label3.TabIndex = 7;
            this.label3.Text = "4 Joueurs";
            // 
            // radioButton3
            // 
            this.radioButton3.Location = new System.Drawing.Point(88, 74);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(16, 15);
            this.radioButton3.TabIndex = 6;
            this.radioButton3.Text = "radioButton";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "3 Joueurs";
            // 
            // radioButton4
            // 
            this.radioButton4.Location = new System.Drawing.Point(88, 97);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(16, 14);
            this.radioButton4.TabIndex = 4;
            this.radioButton4.Text = "radioButton";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "2 Joueurs";
            // 
            // radioButton2
            // 
            this.radioButton2.Checked = true;
            this.radioButton2.Location = new System.Drawing.Point(88, 52);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(16, 15);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "radioButton";
            // 
            // label
            // 
            this.label.Location = new System.Drawing.Point(16, 30);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(48, 15);
            this.label.TabIndex = 1;
            this.label.Text = "1 Joueur";
            // 
            // radioButton
            // 
            this.radioButton.Location = new System.Drawing.Point(88, 30);
            this.radioButton.Name = "radioButton";
            this.radioButton.Size = new System.Drawing.Size(16, 15);
            this.radioButton.TabIndex = 0;
            this.radioButton.Text = "radioButton";
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(64, 201);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(88, 29);
            this.button.TabIndex = 1;
            this.button.Text = "Valider";
            this.button.Click += new System.EventHandler(this.ButtonClick);
            // 
            // NbJoueurForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(208, 266);
            this.Controls.Add(this.button);
            this.Controls.Add(this.groupBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NbJoueurForm";
            this.Text = "Nombre de Joueurs?";
            this.groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

		}
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Button button;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton;
        private System.Windows.Forms.GroupBox groupBox;
        #endregion
    }
}