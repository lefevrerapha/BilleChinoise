﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BillChinoiseWindowsForm
{
    public partial class RegleForm : Form
    {
        public RegleForm()
        {
            InitializeComponent();
        }
		void RegleFormLoad(object sender, System.EventArgs e)
		{
			richTextBox3.Text = "Chaque joueur doit placer toutes ses billes dans l'emplacement du joueur d'en face, avant que celui-ci ne fasse la même chose.";
			richTextBox4.Text = "Les billes ne peuvent être déplacées qu'en diagonal, soit d'une case soit de plusieurs cases, en jouant à saute moutons.\n Un click sur la bille de départ, un click sur les cases vides intermédiaires et enfin deux click sur la case finale.";

		}

		void ButtonClick(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
