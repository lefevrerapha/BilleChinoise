﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BillChinoiseWindowsForm
{
    public partial class NbJoueurForm : Form
    {
        #region properties
        private int ordi = 0;
        private Point Depl;
        #endregion

        #region form function
        public NbJoueurForm()
        {
            InitializeComponent();
        }

        void ButtonClick(object sender, System.EventArgs e)
        {
            this.Visible = false;
        }

        public int getNbJoueurs()
        {
            int nb_Joueurs = 0;
            if (radioButton.Checked) { nb_Joueurs = 1; ordi = 1; Depl = new Point(0, -1); }
            if (radioButton2.Checked) nb_Joueurs = 2;
            if (radioButton3.Checked) { nb_Joueurs = 3; ordi = 1; Depl = new Point(-1, -1); }
            if (radioButton4.Checked) nb_Joueurs = 4;
            if (radioButton5.Checked) { nb_Joueurs = 5; ordi = 1; Depl = new Point(-1, 1); }
            if (radioButton6.Checked) nb_Joueurs = 6;
            return nb_Joueurs;
        }

        public int getNumOrdi()
        {
            return ordi;
        }

       
        #endregion
    }
}
