﻿using BilleChinoiseGame;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BillChinoiseWindowsForm
{
    public partial class GameManagerForm : Form
    {
        #region parameters
        public Game game = null;
        public GraphicalDisplay graphicalDisplay = null;

        public int Joueur; //Joueur qui doit jouer un coup
        private int nb_Joueurs;//Nb de joueur dans la partie
        private int nb_Ai;

        #endregion


        #region form interaction

        public GameManagerForm()
        {
            InitializeComponent();
        }

        void Menu_Quitter(object sender, System.EventArgs e)
        {
            Application.Exit();
        }
        void GameManagerForm_Load(object sender, System.EventArgs e)
        {
            NbJoueurForm NbJoueurs = new NbJoueurForm();
            NbJoueurs.ShowDialog();
            nb_Joueurs = NbJoueurs.getNbJoueurs();
            nb_Ai = NbJoueurs.getNumOrdi();
            NbJoueurs.Close();
            game = new Game(0, nb_Ai+ nb_Joueurs);
            graphicalDisplay = new GraphicalDisplay(game, panel1, label1, AiActionButton);
            game.graphicalDisplay = graphicalDisplay;
            graphicalDisplay.InitDrawing();
            graphicalDisplay.Afficher();
            game.Play();
        }

        void Menu_New(object sender, System.EventArgs e)
        {
            //Interrogation du nombre de joueurs dans la partie
            NbJoueurForm NbJoueurs = new NbJoueurForm();
            DialogResult dialogResult = NbJoueurs.ShowDialog();
            if (dialogResult == DialogResult.Cancel)
                return;
            nb_Joueurs = NbJoueurs.getNbJoueurs();
            nb_Ai = NbJoueurs.getNumOrdi();
            NbJoueurs.Close();
            game = new Game(nb_Joueurs, nb_Ai);
            graphicalDisplay = new GraphicalDisplay(game, panel1, label1, AiActionButton);
            game.graphicalDisplay = graphicalDisplay;
            graphicalDisplay.InitDrawing();
            graphicalDisplay.Afficher();
            game.Play();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            graphicalDisplay.FocusCase(e.X, e.Y);
        }

        #endregion

        #region other fucntion

        #endregion

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (!(graphicalDisplay is null))
                graphicalDisplay.Afficher();
        }

        private void AiActionButton_Click(object sender, EventArgs e)
        {
            if (game.currentPlayer is AiPlayer)
                (game.currentPlayer as AiPlayer).Move(graphicalDisplay, false);
        }

        private void previousState_Click(object sender, EventArgs e)
        {
            game.PreviousState();
            graphicalDisplay.DiplayPlateau();
            graphicalDisplay.DisplayPlayer(game.currentPlayer);
        }
    }
}
