﻿namespace BillChinoiseWindowsForm
{
    partial class RegleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.button = new System.Windows.Forms.Button();
			this.richTextBox2 = new System.Windows.Forms.RichTextBox();
			this.richTextBox3 = new System.Windows.Forms.RichTextBox();
			this.richTextBox4 = new System.Windows.Forms.RichTextBox();
			this.richTextBox = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// button
			// 
			this.button.Location = new System.Drawing.Point(168, 288);
			this.button.Name = "button";
			this.button.Size = new System.Drawing.Size(80, 48);
			this.button.TabIndex = 4;
			this.button.Text = "OK";
			this.button.Click += new System.EventHandler(this.ButtonClick);
			// 
			// richTextBox2
			// 
			this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox2.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic)
							| System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.richTextBox2.ForeColor = System.Drawing.Color.Red;
			this.richTextBox2.Location = new System.Drawing.Point(8, 152);
			this.richTextBox2.Name = "richTextBox2";
			this.richTextBox2.Size = new System.Drawing.Size(184, 24);
			this.richTextBox2.TabIndex = 1;
			this.richTextBox2.Text = "Déplacements";
			// 
			// richTextBox3
			// 
			this.richTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox3.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.richTextBox3.ForeColor = System.Drawing.Color.Blue;
			this.richTextBox3.Location = new System.Drawing.Point(8, 72);
			this.richTextBox3.Name = "richTextBox3";
			this.richTextBox3.Size = new System.Drawing.Size(408, 72);
			this.richTextBox3.TabIndex = 2;
			this.richTextBox3.Text = "But du jeu";
			// 
			// richTextBox4
			// 
			this.richTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox4.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.richTextBox4.ForeColor = System.Drawing.Color.Blue;
			this.richTextBox4.Location = new System.Drawing.Point(8, 184);
			this.richTextBox4.Name = "richTextBox4";
			this.richTextBox4.Size = new System.Drawing.Size(408, 72);
			this.richTextBox4.TabIndex = 3;
			this.richTextBox4.Text = "But du jeu";
			// 
			// richTextBox
			// 
			this.richTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic)
							| System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.richTextBox.ForeColor = System.Drawing.Color.Red;
			this.richTextBox.Location = new System.Drawing.Point(8, 40);
			this.richTextBox.Name = "richTextBox";
			this.richTextBox.Size = new System.Drawing.Size(184, 24);
			this.richTextBox.TabIndex = 0;
			this.richTextBox.Text = "But du jeu";
			// 
			// RegleForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(424, 357);
			this.Controls.Add(this.button);
			this.Controls.Add(this.richTextBox4);
			this.Controls.Add(this.richTextBox3);
			this.Controls.Add(this.richTextBox2);
			this.Controls.Add(this.richTextBox);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "RegleForm";
			this.Text = "Règles du jeu";
			this.Load += new System.EventHandler(this.RegleFormLoad);
			this.ResumeLayout(false);
		}
        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Button button;
        #endregion
    }
}